clear();

interest_date = {'Jun20',};
in_directory_prefix = 'H:\wifi_test\test_data\';
out_directory_prefix = 'H:\wifi_test\parsed_data\';
nrx = 3; 
ntx = 2;
n_subcarriers = 56;
skip_time = 0; % in seconds
duration = 0.3; % in seconds
min_num = 300;
max_dur = 0.16; % in seconds
min_dur = 0.07;
step_size = 10;
start_offset = 2;
num_jumps = 0;

for date_cell = interest_date
    date = date_cell{1}; %convert from cell to string
    fprintf('processing data from %s\n', date);
    date_conf = test_date_conf(date);
    
    clear static_data_H;
    clear human_data_H;
    
    % processing static data
    static_test_ids = date_conf.static;
    static_data_H(length(static_test_ids)) = struct;
    for test_index = static_test_ids
        frame_data = read_log_file(strcat(in_directory_prefix, date, '\still', int2str(test_index), '.data'));
        timestamps = zeros(size(frame_data,1),1);
        channel = zeros(size(frame_data,1), nrx, ntx, n_subcarriers);
        for m =1:size(frame_data,1)
            timestamps(m, 1) = frame_data{m}.timestamp;
            channel(m, :, :, :) = frame_data{m}.csi;
        end  
        for m = 1:size(frame_data,1)
            if timestamps(m,1)-timestamps(1,1)>skip_time*1e6
                start_offset = m;
                break;
            end
        end
        m = start_offset;
        num_jumps = 0;
        prev_jump_index = 1;
        frame_interval = [];
        jump_index = [];
        total_count = size(frame_data,1);
        while m<total_count
            if timestamps(m,1)-timestamps(m-1,1)>0.5*1e6
                this_frame_count = m-1-prev_jump_index;
                if this_frame_count>min_num 
                    this_dur = timestamps(prev_jump_index+min_num-1,1)-timestamps(prev_jump_index, 1);
                    if this_dur < max_dur*1e6 && this_dur > min_dur*1e6
                        frame_interval = [frame_interval, this_dur];
                        jump_index = [jump_index, m];
                    end
                end                
                num_jumps = num_jumps+1;
                prev_jump_index = m;
            end
            m = m+1;
        end
        fprintf('static set %d, num of jumps %d, num of valid image %d\n', test_index, num_jumps, size(frame_interval,2));
%         fprintf('static set %d, valid image num %d, invalid image num %d, total frame %d, samllest dur %f, largest dur %f\n', ...
%             test_index, valid_image, invalid_image, size(frame_data,1), smallest_dur, largest_dur);    
        figure(); 
        plot(timestamps(1:total_count)); hold on;
        plot(jump_index, timestamps(jump_index), 'o');
                
    end
    
%     % processing human movement data
%     human_test_ids = date_conf.human;
%     human_data_H(length(human_test_ids)) = struct;
%     for test_index = human_test_ids
%         frame_data = read_log_file(strcat(in_directory_prefix, date, '\walking', int2str(test_index), '.data'));
%         human_data_H(test_index).timestamps = zeros(size(frame_data,1),1);
%         human_data_H(test_index).channel = zeros(size(frame_data,1), nrx, ntx, n_subcarriers);
%         for m =1:size(frame_data,1)
%             human_data_H(test_index).timestamps(m, 1) = frame_data{m}.timestamp;
%             human_data_H(test_index).channel(m, :, :, :) = frame_data{m}.csi;
%         end       
%     end
end

