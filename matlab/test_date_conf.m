function data_set = test_date_conf( interest_data )
test_conf = containers.Map();
test_conf(strcat('Jun', int2str(20))) = struct('static', 1:5, 'human', 1:5);
test_conf(strcat('Jun', int2str(21))) = struct('static', 1:45, 'human', 1:45);
data_set = test_conf(interest_data);
end

