/*
 * =====================================================================================
 *       Filename:  main.c
 *
 *    Description:  Here is an example for receiving CSI matrix 
 *                  Basic CSi procesing fucntion is also implemented and called
 *                  Check csi_fun.c for detail of the processing function
 *        Version:  1.0
 *
 *         Author:  Yaxiong Xie
 *         Email :  <xieyaxiongfly@gmail.com>
 *   Organization:  WANDS group @ Nanyang Technological University
 *   
 *   Copyright (c)  WANDS group @ Nanyang Technological University
 * =====================================================================================
 */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <termios.h>
#include <pthread.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>
#include "csi_fun.h"
#include <sys/socket.h>
#include <netdb.h>
#include <arpa/inet.h>

#define BUFSIZE 4096

int quit, connect_status;
int sockfd;
unsigned char buf_addr[BUFSIZE];
unsigned char data_buf[1500];

COMPLEX csi_matrix[3][3][114];
csi_struct*   csi_status;

void sig_handler(int signo)
{
    if (signo == SIGINT){
        printf("keyboard Interrupt\n");
        quit = 1;
    }
    if (signo == SIGPIPE){
        connect_status = 0;
        printf("broken pipe!\n");
        close(sockfd);
        printf("closed sockets\n");
    }
}

int send_all(int sock, const void *buf, int len){
    const char *pbuf = (const char*) buf;
    while(len>0){
        int sent = send(sock, pbuf, len, 0);
        if (sent<1){
            printf("can't write to socket\n");
            return -1;
        }
        pbuf += sent;
        len -= sent;
    }
    return 0;

}


int main(int argc, char* argv[])
{
    FILE*       fp;
    int         fd;
    int         i;
    int         total_msg_cnt,cnt;
    unsigned char endian_flag;
    u_int16_t   buf_len;
    int  less_antenna=0;
    connect_status = 0;
    struct sockaddr_in servaddr;
    signal(SIGPIPE, sig_handler);
    signal(SIGINT, sig_handler);
    int send_to_socket = 0;
    int send_to_file = 0;
    int file_closed = 0;
    csi_status = (csi_struct*)malloc(sizeof(csi_struct));
    quit = 0;
    /* check usage */
    if (1 == argc){
        /* If you want to log the CSI for off-line processing,
         * you need to specify the name of the output file
         */
        printf("/**************************************/\n");
        printf("/*   Usage: recv_csi <output_file>    */\n");
        printf("/**************************************/\n");
    }
    if ((2 == argc)||(3==argc)){
        if (atoi(argv[1])){
            sockfd = socket(AF_INET, SOCK_STREAM,0);
            if (sockfd==-1){
                printf("socket creating failed...\n");
                return 0;
            }
            else{
                printf("socket successfully created..\n");
            }
            send_to_socket = 1;
            bzero(&servaddr, sizeof(servaddr));
            servaddr.sin_family = AF_INET;
            servaddr.sin_addr.s_addr = inet_addr("127.0.0.1");
            servaddr.sin_port = htons(4555);
            while(1){
                if (1 == quit){
                    break;
                }

               if (connect(sockfd, (struct sockaddr*)&servaddr, sizeof(servaddr))!=0){
                    printf("connection with the server failed...\n");
                }
                else{
                    printf("connected to the server..\n");
                    connect_status=1;
                    break;
                }
                sleep(1.0);
            }
        }
        else{
            printf("sockes disabled\n");
        }  
    }
    if (3 == argc){
        fp = fopen(argv[2],"w");
        if (!fp){
            printf("Fail to open <output_file>, are you root?\n");
            fclose(fp);
            return 0;
        }
        send_to_file = 1;
    }
    if (argc > 3){
        printf(" Too many input arguments !\n");
        return 0;
    }

    fd = open_csi_device();
    if (fd < 0){
        perror("Failed to open the device...");
        return errno;
    }
    
    printf("#Receiving data! Press Ctrl+C to quit!\n");

    total_msg_cnt = 0;
    
    while(1){
        if (1 == quit){
            break;
        }

        /* keep listening to the kernel and waiting for the csi report */
        printf("read csi buffer\n");
        cnt = read_csi_buf(buf_addr,fd,BUFSIZE);
        printf("total bytes read %d\n", cnt);
        if (cnt){
            total_msg_cnt += 1;

            /* fill the status struct with information about the rx packet */
            printf("recording packet status\n");
            record_status(buf_addr, cnt, csi_status);
            printf("done\n");
            if (csi_status->nc<3){
		        less_antenna = less_antenna+1;
	        }
	        printf("\n----START OF FRAME----\n");

	        printf("less antenna count %d\n", less_antenna);
            printf("Message Seq: %d\n", total_msg_cnt);
            printf("HW Timestamp: %"PRIu64"\n", csi_status->tstamp);
            printf("Channel: %d\n", csi_status->channel);
            printf("Bandwidth: %d\n", csi_status->chanBW);
            printf("PHY Rate: 0x%02x | #of RX antenna: %d | #of TX antenna: %d | #of subcarriers: %d \
                    | Noise Floor: %d\n", csi_status->rate,csi_status->nr,csi_status->nc,csi_status->num_tones,csi_status->noise);
            printf ("PHY Error Code: %d\n",csi_status->phyerr);
            printf ("RX RSSI: %d | ctrl chain 0,1,2: %d, %d, %d\n", csi_status->rssi,csi_status->rssi_0,csi_status->rssi_1,csi_status->rssi_2);
            printf("Payload len: %d | CSI len: %d | Buffer len: %d\n",csi_status->payload_len,csi_status->csi_len, csi_status->buf_len);
            printf("----END OF FRAME----\n");
             /* 
             * fill the payload buffer with the payload
             * fill the CSI matrix with the extracted CSI value
             */
            if ((csi_status->nr>3)||(csi_status->nr<0)){
                printf("invalid nr %d\n", csi_status->nr);
                continue;
            }
            if ((csi_status->nc>3)||(csi_status->nc<0)){
                printf("invalid nc %d\n", csi_status->nc);
                continue;
            }
            if ((csi_status->num_tones>114)||(csi_status->num_tones<0)){
                printf("invalid num_tones %d\n", csi_status->num_tones);
                continue;
            }
            if ((csi_status->payload_len>1500)||(csi_status->payload_len<0)){
                printf("invalid payload_len %d\n", csi_status->payload_len);
                continue;
            }
 
            int expected_csi_len = csi_status->nr*csi_status->nc*csi_status->num_tones*2*10/8;
            if (csi_status->csi_len!=expected_csi_len){
                printf("invalid csi len %d\n", csi_status->csi_len);
                continue;
            }


            printf("recording csi payload\n");
            record_csi_payload(buf_addr, csi_status, data_buf, csi_matrix); 
            printf("done\n");
  
            /* Till now, we store the packet status in the struct csi_status 
             * store the packet payload in the data buffer
             * store the csi matrix in the csi buffer
             * with all those data, we can build our own processing function! 
             */
            
            
            //porcess_csi(data_buf, csi_status, csi_matrix);   
            /* log the received data for off-line processing */
            if (send_to_socket||send_to_file){
                // ignore payload field
                if (csi_status->buf_len==0){
                    printf("buffer length is zero\n");
                    continue;
                }
                else if (csi_status->buf_len <= csi_status->payload_len){
                    printf("buffer length is less than or equal to payload length\n");
                    continue;
                }
                else{
                    buf_len = csi_status->buf_len - csi_status->payload_len;
                     if (send_to_file){
                        //if ((total_msg_cnt>1080000)&&(total_msg_cnt<1262000)){
                        if (true){
                            printf("sending to file\n");
                            fwrite(&buf_len,1,2,fp);
                            fwrite(buf_addr,1,buf_len,fp);
                            printf("done\n");
                        }
                        else if ((total_msg_cnt>1262000)&&(file_closed==0)){
                            int r = fclose(fp);
                            if (r!=0){
                                printf("file stream closed unsuccessfully\n");
                            }
                            else{
                                file_closed = 1;
                            }
                            
                        }
                    }
                    if (send_to_socket&&connect_status){
                        int result;
                        printf("sending to socket\n");
                        result = send_all(sockfd, &buf_len, sizeof(buf_len));
                        if (result!=0){
                            connect_status = 0;
                            close(sockfd);
                        }
                        else{
                            result = send_all(sockfd, buf_addr, buf_len);
                            if (result!=0){
                                connect_status = 0;
                                close(sockfd);
                            }
                            else
                                printf("done\n");
                        }
                    }
                }
            }
            if (send_to_socket&&(!connect_status)){
                sockfd = socket(AF_INET, SOCK_STREAM,0);
                if (sockfd==-1){
                    printf("socket creating failed...\n");
                    return 0;
                }
                else{
                    printf("socket successfully created..\n");
                }
                if (connect(sockfd, (struct sockaddr*)&servaddr, sizeof(servaddr))!=0){
                    printf("connection with the server failed...\n");
                }
                else{
                    printf("connected to the server..\n");
                    connect_status=1;
                }
 
            }
        }
        printf("\n");
    }
    int last;
    if (send_to_socket){
        close(sockfd);
        printf("closed client socket\n");
    }
    if (send_to_file&&(file_closed==0)){
        last = fclose(fp);
        if (last!=0){
            printf("file stream closed unsuccessfully\n");
        }
    }
    close_csi_device(fd);
    free(csi_status);
    return 0;
}
