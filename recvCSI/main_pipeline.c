/*
 * =====================================================================================
 *       Filename:  main.c
 *
 *    Description:  Here is an example for receiving CSI matrix 
 *                  Basic CSi procesing fucntion is also implemented and called
 *                  Check csi_fun.c for detail of the processing function
 *        Version:  1.0
 *
 *         Author:  Yaxiong Xie
 *         Email :  <xieyaxiongfly@gmail.com>
 *   Organization:  WANDS group @ Nanyang Technological University
 *   
 *   Copyright (c)  WANDS group @ Nanyang Technological University
 * =====================================================================================
 */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <termios.h>
#include <pthread.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>
#include "csi_fun.h"

#define BUFSIZE 4096

int quit;
unsigned char buf_addr[BUFSIZE];
unsigned char data_buf[1500];

COMPLEX csi_matrix[3][3][114];
csi_struct*   csi_status;

void sig_handler(int signo)
{
    if (signo == SIGINT)
        quit = 1;
}

int main(int argc, char* argv[])
{
    FILE*       fp;
    int         fd;
    int         i;
    int         total_msg_cnt,cnt;
    int         log_flag;
    unsigned char endian_flag;
    u_int16_t   buf_len;
    int  less_antenna=0;
    
    log_flag = 1;
    csi_status = (csi_struct*)malloc(sizeof(csi_struct));
    /* check usage */
    if (1 == argc){
        /* If you want to log the CSI for off-line processing,
         * you need to specify the name of the output file
         */
        log_flag  = 0;
        printf("/**************************************/\n");
        printf("/*   Usage: recv_csi <output_file>    */\n");
        printf("/**************************************/\n");
    }
    if (2 == argc){
        if (atoi(argv[1])){
            fp = popen("python3 ~/Desktop/Repo/codes/socket_client.py", "w");
            if (!fp){
                printf("Fail to start python program\n");
                pclose(fp);
                return 0;
            }
            log_flag = 1;
        }  
    }
    if (3 == argc){
        fp = fopen(argv[1],"w");
        if (!fp){
            printf("Fail to open <output_file>, are you root?\n");
            fclose(fp);
            return 0;
        }
        log_flag = 2; 
    }
    if (argc > 3){
        printf(" Too many input arguments !\n");
        return 0;
    }

    fd = open_csi_device();
    if (fd < 0){
        perror("Failed to open the device...");
        return errno;
    }
    
    printf("#Receiving data! Press Ctrl+C to quit!\n");

    quit = 0;
    total_msg_cnt = 0;
    
    while(1){
        if (1 == quit){
            int last; 
            if (log_flag==1){
               last = pclose(fp);
               if (last==-1)
                printf("pipe stream closed unsuccessfully\n");
            }
            else{
               last = fclose(fp);
               if (last!=0)
                printf("file stream closed unsuccessfully\n");
            } 
            close_csi_device(fd);
            free(csi_status);
            return 0;
        }

        /* keep listening to the kernel and waiting for the csi report */
        cnt = read_csi_buf(buf_addr,fd,BUFSIZE);

        if (cnt){
            total_msg_cnt += 1;

            /* fill the status struct with information about the rx packet */
            record_status(buf_addr, cnt, csi_status);

            /* 
             * fill the payload buffer with the payload
             * fill the CSI matrix with the extracted CSI value
             */
            record_csi_payload(buf_addr, csi_status, data_buf, csi_matrix); 
            
            /* Till now, we store the packet status in the struct csi_status 
             * store the packet payload in the data buffer
             * store the csi matrix in the csi buffer
             * with all those data, we can build our own processing function! 
             */
            //porcess_csi(data_buf, csi_status, csi_matrix);   
            if (csi_status->nc<3){
		        less_antenna = less_antenna+1;
	        }
	        printf("\n----START OF FRAME----\n");

	        printf("less antenna count %d\n", less_antenna);
            printf("Message Seq: %d\n", total_msg_cnt);
            printf("HW Timestamp: %"PRIu64"\n", csi_status->tstamp);
            printf("Channel: %d\n", csi_status->channel);
            printf("Bandwidth: %d\n", csi_status->chanBW);
            printf("PHY Rate: 0x%02x | #of RX antenna: %d | #of TX antenna: %d | #of subcarriers: %d \
                    | Noise Floor: %d\n", csi_status->rate,csi_status->nr,csi_status->nc,csi_status->num_tones,csi_status->noise);
            printf ("PHY Error Code: %d\n",csi_status->phyerr);
            printf ("RX RSSI: %d | ctrl chain 0,1,2: %d, %d, %d\n", csi_status->rssi,csi_status->rssi_0,csi_status->rssi_1,csi_status->rssi_2);
            printf("Payload len: %d | CSI len: %d | Buffer len: %d\n",csi_status->payload_len,csi_status->csi_len, csi_status->buf_len);
            printf("----END OF FRAME----\n");
              /* log the received data for off-line processing */
            if (log_flag){
                // ignore payload field
                if (csi_status->buf_len==0){
                    printf("buffer length is zero\n");
                    continue;
                }
                else if (csi_status->buf_len <= csi_status->payload_len){
                    printf("buffer length is less than or equal to payload length\n");
                    continue;
                }
                else{
                    buf_len = csi_status->buf_len - csi_status->payload_len;
                    fwrite(&buf_len,1,2,fp);
                    fwrite(buf_addr,1,buf_len,fp);
                    if (log_flag==1){
                        fprintf(fp, "\n");
                        printf("send to python %d\n", buf_len+2);
		                fflush(fp);
                    }
                }
            }
        }
    }
    int last;
    if (log_flag==1){
        last = pclose(fp);
        if (last==-1){
            printf("pipe stream closed unsuccessfully\n");
        }
    }
    else{
        last = fclose(fp);
        if (last!=0){
            printf("file stream closed unsuccessfully\n");
        }
    }
    close_csi_device(fd);
    free(csi_status);
    return 0;
}
