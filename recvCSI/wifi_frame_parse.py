#!/usr/bin/python3

import numpy as np
from struct import unpack, calcsize
import collections
import time


class ParseDataFile:
    def __init__(self):
        self.fmt = '<HQHHBBBBBBBBBBBH'
        self.fmt_sz = calcsize(self.fmt)
        self.bit_per_byte = 8
        self.bit_per_symbol = 10 # highest bit is the sign
        self.packet_format = collections.namedtuple('packet_format', 'field_len, timestamp, csi_len, tx_channel, '
                                                                'err_info, noise_floor, rate, bw, num_tones,'
                                                                'nr, nc, rssi, rssi1, rssi2, rssi3, payload_len')

    def parse(self, filename):
        print("reading file "+filename)
        byte_file = np.fromfile(filename, np.uint8)
        file_size = byte_file.shape[0]
        frame_data = {}
        count = 1
        offset = 0
        while offset < file_size-4:
            current_format = self.packet_format._make(unpack(self.fmt,
                                                             byte_file[offset:offset+self.fmt_sz]))
            if (offset + current_format.field_len) > file_size-4:
                break
	    
	    offset += self.fmt_sz
            num_tones = current_format.num_tones
            nc = current_format.nc
            nr = current_format.nr
            csi_len = current_format.csi_len
            if csi_len > 0:
                csi_byte = byte_file[offset:offset+csi_len]
                csi_bits = np.unpackbits(csi_byte)
                csi_bits = np.reshape(csi_bits, (8, int(len(csi_bits)/8)), order='F')
                permutation = range(7, -1, -1)
                csi_bits = csi_bits[permutation, :]
                csi_bits = np.reshape(csi_bits, (self.bit_per_symbol, int(csi_len*8/self.bit_per_symbol)), order='F')
                csi_num = np.zeros((1, nr*nc*num_tones*2), np.float32)
                csi_bits = csi_bits.astype(np.uint16)
                for i in range(self.bit_per_symbol):
                    csi_num[0, :] += (csi_bits[i, :] << i)
                csi_num[0, :] -= (csi_bits[self.bit_per_symbol-1, :]*(1 << self.bit_per_symbol))
                csi = csi_num[0, 1::2] + 1j*csi_num[0, 0::2]
                csi = np.reshape(csi, (nr, nc, num_tones), order='F')
                offset += csi_len
		#print("frame num " + str(count) + "has timestamp " + str(current_format.timestamp))
                frame_data[count] = {"format": current_format,
                                     "csi": csi}
                count += 1

            offset += current_format.payload_len

            if offset + 420 > file_size:
                break

        print("finished parsing file, total number of valid frame (has csi): "+str(len(frame_data)))
        return frame_data


def main():
    ###############################
    # read data from .data log file
    ################################
    parser = ParseDataFile()
    filename = 'AP_test/csi_data.data'
    frame_data = parser.parse(filename+str(i))
    # find about total number of frames in frame_data:
    # len(frame_data)
    # access ith (i=[1-len(frame_data)]) frame's csi
    # frame_data[i]['csi']
    # access ith (i=1-len(frame_data)) frame's timestamp
    # frame_data[i]['format'].timestamp

    ###############################################################
    # save data to file:
    # suppose the N-D data you prepare for the neural network is A,
    # and data type of A is np.float32, the first dimension is number of samples
    # and you want to save A to disk such that you can use to it to
    # neural network directly without doing all the pre-processing again
    # run:
    # A.tofile(data_filename)
    # read data from data_filename
    # B = np.fromfile(data_filename, dtype=np.float32)
    # B = np.reshape(B, (-1, )+A.shape[1:])
    ##############################################################


if __name__ == "__main__":
    main()
